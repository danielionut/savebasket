angular.module('SaveBasket.Templates', ['modals/templates/emptyBasketModal.tpl.html', 'modals/templates/infoOverlay.tpl.html', 'modals/templates/offerModal.tpl.html', 'modals/templates/loginOverlay.tpl.html', 'modals/templates/noOfferOverlay.tpl.html', 'modals/templates/replaceOverlay.tpl.html', 'offers/templates/sbOfferDisplay.tpl.html']);

angular.module('modals/templates/emptyBasketModal.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/emptyBasketModal.tpl.html',
    '<div class="sb-modal"><div class="sb-modal__container"><div class="desktop-wrapper-narrow"><div class="sb-modal__header"><div class="sb-modal__close" ng-click="close()"></div><div class="sb-modal__title"><span class="sb-hart"></span> <span custom-text="MCSGOURL.167">Je favorieten</span></div></div><hr><div class="sb-modal__content"><div class="sb-img-empty-basket center box-margin-vertical box-margin-center"></div><p class="center"><span custom-text="MCSGOURL.169">Hmmmm...<br>heb je nog niets in je favorieten?</span></p><div class="center box-margin-top"><button sb-trigger="save" class="btn btn-magenta"><span custom-text="MCSGOURL.168">Huidige keuze opslaan</span></button></div></div></div></div></div>');
}]);

angular.module('modals/templates/infoOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/infoOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent sb-info-modal"><div class="sb-modal__container"><div class="desktop-wrapper"><div sb-offer-count sb-trigger class="trigger-desktop"><span class="sb-hart"></span> <span class="sb-trigger-static-text">Favorieten</span></div><div class="sb-modal__content"><div class="content__header"><div class="sb-img-arrow-heart"></div><p class="sb-text-button-right text-styles text"><span custom-text="MCSGOURL.171">Hier vind je jouw favorieten</span></p><div class="sb-text-button-right text-styles"><button ng-click="close()" class="btn btn-magenta"><span custom-text="MCSGOURL.170">Ok&eacute;, ik snap het!</span></button></div></div></div></div></div></div>');
}]);

angular.module('modals/templates/offerModal.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/offerModal.tpl.html',
    '<div class="sb-modal sb-offer-modal"><div class="sb-modal__container"><div class="desktop-wrapper-narrow"><div class="sb-modal__header"><div class="sb-modal__close" ng-click="close()"></div><div class="sb-modal__title"><span class="sb-hart"></span> <span custom-text="MCSGOURL.167">Je favorieten</span></div></div><hr><div class="sb-modal__content"><sb-offer-display ng-repeat="offer in offers track by $index" offer="offer"></sb-offer-display></div></div></div></div>');
}]);

angular.module('modals/templates/loginOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/loginOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent"><div class="sb-modal__container"><div class="sb-modal__content"><p class="center bold">Om iets op te slaan in je eigen verlanglijstje moet je ingelogd zijn.</p><div class="center"><button ng-click="login()" class="btn btn-magenta">Login</button></div><div class="center"><button ng-click="close()" class="btn btn-inverse-blue box-margin-top">Ga verder</button></div></div></div></div>');
}]);

angular.module('modals/templates/noOfferOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/noOfferOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent"><div class="sb-modal__container"><div class="sb-modal__content center-content"><p class="center"><span custom-text="MCSGOURL.177">Om iets op te slaan op je favorieten moet je eerst een toestel selecteren</span></p><div class="center box-margin-top"><button ng-click="close()" class="btn btn-magenta"><span custom-text="MCSGOURL.170">Ok&eacute;, ik snap het!</span></button></div></div></div></div>');
}]);

angular.module('modals/templates/replaceOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/replaceOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent"><div class="sb-modal__container"><div class="sb-modal__content center-content"><div class="box-margin-vertical sb-img-offer-replace box-margin-center"></div><p class="center offer-replace-text"><span custom-text="MCSGOURL.173">Je hebt al een artikel in<br>je favorieten</span></p><p class="center"><span custom-text="MCSGOURL.174">Wil je die vervangen?</span></p><div class="center box-margin-top offer-replace-btn"><button ng-click="close()" class="btn btn-grey">Nee</button> <button sb-trigger="replace" class="btn btn-green">Ja</button></div></div></div></div>');
}]);

angular.module('offers/templates/sbOfferDisplay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('offers/templates/sbOfferDisplay.tpl.html',
    '<div class="sb-offer box-padding-vertical"><div class="sb-offer__image"><img ng-src="{{ offer.image }}" alt=""></div><div class="sb-offer__info"><div class="sb-offer__title"><h2>{{ offer.title }}</h2></div><div class="box-margin-top sb-offer__info-rateplan"><span class="magenta small">Abonnement&nbsp;{{ offer.rateplan.duration | number:0 }}&nbsp;mnd</span></div><div ng-if="offer.data.abodat" class="sb-offer__half box-margin-top"><p><span class="bold large" ng-bind="offer.data.abodat"></span> <span class="bold small">Internet</span> <span ng-if="offer.rateplan.showTShirtSize && offer.data.tshirtSize">({{offer.data.tshirtSize}})</span></p><p><span class="tiny grey" ng-bind="offer.data.abodatdown"></span></p></div><div ng-if="offer.minutes.abobmin" class="sb-offer__half box-margin-top"><p><span class="bold large" ng-bind="offer.minutes.abobmin"></span> <span class="bold large suffix" ng-bind="offer.minutes.suffix"></span></p><p><span class="tiny grey" ng-bind="offer.minutes.abobun"></span></p></div><div class="sb-offer__list box-margin-top"><ul><li ng-if="offer.insurance.description"><span class="tiny grey" ng-bind="offer.insurance.description"></span></li><li ng-repeat="addon in offer.addons"><span class="tiny grey" ng-bind="addon.name"></span></li></ul></div><div class="sb-offer__price box-margin-top"><p ng-if="offer.totalPrice"><span class="green">&euro;&nbsp;{{offer.totalPrice}}</span> <span class="green">/mnd</span></p><p class="small" ng-if="offer.handsetTotal"><span class="bold">Bijbetaling&nbsp;&euro;&nbsp;</span> <span class="bold" ng-bind="offer.handsetTotal"></span></p></div></div><div class="sb-offer__commands box-margin-top"><button class="sb-offer__commands--order btn btn-magenta" ng-click="orderOffer(offer)"><span custom-text="MCSGOURL.166">Bestellen</span></button> <button class="sb-offer__commands--edit btn btn-inverse-blue" ng-click="changeOffer(offer)"><span custom-text="MCSGOURL.165">Wijzig</span></button></div></div><hr>');
}]);
