angular.module('SaveBasket.Templates', ['modals/templates/loginOverlay.tpl.html', 'modals/templates/emptyBasketModal.tpl.html', 'modals/templates/infoOverlay.tpl.html', 'modals/templates/offerModal.tpl.html', 'modals/templates/noOfferOverlay.tpl.html', 'modals/templates/replaceOverlay.tpl.html', 'offers/templates/sbOfferDisplay.tpl.html']);

angular.module('modals/templates/loginOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/loginOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent"><div class="sb-modal__container"><div class="sb-modal__content"><p class="center bold">Om iets op te slaan in je eigen verlanglijstje moet je ingelogd zijn.</p><div class="center"><button ng-click="login()" class="btn btn-magenta">Login</button></div><div class="center"><button ng-click="close()" class="btn btn-inverse-blue box-margin-top">Ga verder</button></div></div></div></div>');
}]);

angular.module('modals/templates/emptyBasketModal.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/emptyBasketModal.tpl.html',
    '<div class="sb-modal"><div class="sb-modal__container"><div class="sb-modal__header"><div class="sb-modal__close" ng-click="close()"></div><div class="sb-modal__title"><span class="sb-hart"></span> <span custom-text="MCSGOURL.167">Je favorieten</span></div></div><hr><div class="sb-modal__content"><div class="sb-img-empty-basket center box-margin-vertical box-margin-center"></div><p class="center"><span custom-text="MCSGOURL.169">Hmmmm...<br>heb je nog niets in je favorieten?</span></p><div class="center box-margin-top"><button sb-trigger="save" class="btn btn-magenta"><span custom-text="MCSGOURL.168">Huidige keuze opslaan</span></button></div></div></div></div>');
}]);

angular.module('modals/templates/infoOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/infoOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent sb-info-modal"><div class="sb-modal__container"><div sb-offer-count sb-trigger class="sb-trigger"><span class="sb-hart"></span></div><div class="sb-modal__content"><div class="content__header"><div class="sb-img-arrow-heart"></div><p class="center"><span custom-text="MCSGOURL.171">Hier vind je jouw favorieten</span></p></div><div class="center"><button ng-click="close()" class="btn btn-magenta"><span custom-text="MCSGOURL.170">Ok&eacute;, ik snap het!</span></button></div><div class="content__footer"><p class="center"><span custom-text="MCSGOURL.172">....of rond direct af</span></p><div class="sb-img-arrow-order"></div></div></div></div></div>');
}]);

angular.module('modals/templates/offerModal.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/offerModal.tpl.html',
    '<div class="sb-modal"><div class="sb-modal__container"><div class="sb-modal__header"><div class="sb-modal__close" ng-click="close()"></div><div class="sb-modal__title"><span class="sb-hart"></span> <span custom-text="MCSGOURL.167">Je favorieten</span></div></div><hr><div class="sb-modal__content"><sb-offer-display ng-repeat="offer in offers track by $index" offer="offer"></sb-offer-display></div></div></div>');
}]);

angular.module('modals/templates/noOfferOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/noOfferOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent"><div class="sb-modal__container"><div class="sb-modal__content center-content"><p class="center"><span custom-text="MCSGOURL.177">Om iets op te slaan op je favorieten moet je eerst een toestel selecteren</span></p><div class="center box-margin-top"><button ng-click="close()" class="btn btn-magenta"><span custom-text="MCSGOURL.170">Ok&eacute;, ik snap het!</span></button></div></div></div></div>');
}]);

angular.module('modals/templates/replaceOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/replaceOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent"><div class="sb-modal__container"><div class="sb-modal__content center-content"><div class="box-margin-vertical sb-img-offer-replace box-margin-center"></div><p class="center offer-replace-text"><span custom-text="MCSGOURL.173">Je hebt al een artikel in<br>je favorieten</span></p><p class="center"><span custom-text="MCSGOURL.174">Wil je die vervangen?</span></p><div class="center box-margin-top offer-replace-btn"><button ng-click="close()" class="btn btn-grey">Nee</button> <button sb-trigger="replace" class="btn btn-green">Ja</button></div></div></div></div>');
}]);

angular.module('offers/templates/sbOfferDisplay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('offers/templates/sbOfferDisplay.tpl.html',
    '<div class="sb-offer sb-offer-error box-padding-vertical" ng-if="offer.error" ng-switch="offer.error.id"><h2 ng-switch-when="1">{{ offer.error.internalMessage }}</h2></div><div class="sb-offer box-padding-vertical" ng-if="!offer.error"><div class="sb-offer__image"><img ng-src="{{ offer.image }}" alt="" ng-click="changeOffer(offer)"></div><div class="sb-offer__info" ng-click="changeOffer(offer)"><h2>{{ offer.title }}</h2><div class="box-margin-top sb-offer__info-rateplan"><span class="magenta small">Abonnement&nbsp;{{ offer.rateplan.duration | number:0 }}&nbsp;mnd</span></div><div ng-if="offer.data.abodat" class="sb-offer__half box-margin-top"><p><span class="bold large" ng-bind="offer.data.abodat"></span> <span class="bold small">Internet</span> <span ng-if="offer.rateplan.drlh_show_size && offer.data.drlh_size">({{offer.data.drlh_size}})</span></p><p><span class="tiny grey" ng-bind="offer.data.abodatdown"></span></p></div><div ng-if="offer.minutes.abobun" class="sb-offer__half box-margin-top"><p><span class="bold large" ng-bind="offer.minutes.abobmin"></span> <span class="bold small" ng-if="offer.minutes.abobmin !== \'Onbeperkt\'">Minuten</span></p><p><span class="tiny grey" ng-bind="offer.minutes.abobun"></span></p></div><div class="sb-offer__list box-margin-top"><ul><li ng-if="offer.insurance[0].products[0]"><span class="tiny grey" ng-bind="offer.insurance[0].products[0].description"></span></li><li ng-if="offer.insurance.description"><span class="tiny grey" ng-bind="offer.insurance.description" ng-class="{strike: !offer.insurance.available}"></span></li><li ng-repeat="addon in offer.receipt.ratePlan.addOns" ng-if="offer.receipt.ratePlan.addOns"><span class="tiny grey" ng-bind="addon.name"></span></li><li ng-repeat="addon in offer.addOns" ng-if="offer.addOns"><span class="tiny grey" ng-bind="addon.name" ng-class="{strike: !addon.available}"></span></li></ul></div><div class="sb-offer__price box-margin-top"><p ng-if="offer.receipt.finalPricing.monthlyTotal"><span class="magenta" ng-bind="offer.receipt.finalPricing.monthlyTotal + offer.insurance[0].products[0].monthlyFee + offer.insurance.monthlyFee | euroCurrency"></span> <span class="small magenta" custom-label="RAPRD_MPRICE">/mnd</span></p><p class="small" ng-if="offer.receipt.finalPricing.oneoffHandsetTotal"><span>Bijbetaling&nbsp;</span> <span ng-bind="offer.receipt.finalPricing.oneoffHandsetTotal | euroCurrency"></span></p></div></div><div class="sb-offer__commands box-margin-top"><button class="sb-offer__commands--edit btn btn-inverse-blue" ng-click="changeOffer(offer)"><span custom-text="MCSGOURL.165">Wijzig</span></button> <button class="sb-offer__commands--order btn btn-magenta" ng-click="orderOffer(offer)"><span custom-text="MCSGOURL.166">Bestellen</span></button></div></div><hr>');
}]);
