(function () {

    'use strict';

    angular.module('SaveBasket.Offers', [
        'SaveBasket.Offers.Parser'
    ]);

})();