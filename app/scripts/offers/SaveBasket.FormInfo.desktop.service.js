/**
 * Created by sanderrombouts on 11/01/16.
 */
(function () {

    'use strict';

    angular.module('SaveBasket.Offers')
        .factory('sbFormInfo', SaveBasketFormInfo);

    //SaveBasketOffersService.$inject = ['sbOfferParser', 'sbResources'];

    function SaveBasketFormInfo() {


        function getFormValues() {

            var form = {};
            $('#form input').each(function() {
                var $this = $(this);
                form[$this.attr('name')] = $this.val();
            });

            return form;
        }

        function getKey(key) {
            return getFormValues()[key];
        }
        function hasKey(key) {
            return !!getFormValues()[key];
        }


        return {
            get: getKey,
            hasKey: hasKey,
            getAll: getFormValues
        };

    }


})();