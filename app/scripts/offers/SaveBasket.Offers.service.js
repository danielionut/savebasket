(function () {

    'use strict';

    angular.module('SaveBasket.Offers')
        .factory('sbOffers', SaveBasketOffersService);

    //SaveBasketOffersService.$inject = ['sbOfferParser', 'sbResources'];

    function SaveBasketOffersService(sbOfferParser, sbResources, x2js, $rootScope, SB_EVENT, SB_ENV, SB_PERSISTENCE, sbSessionPersistence) {

        var offers         = [],
            maxOffers      = 1,
            selectedOffer,
            busyRetrieving = false;

        var navigateToOffer = function (offer) {
            sbOfferParser.navigateToOffer(offer);
        };

        var orderOffer = function (offer) {
            sbOfferParser.orderOffer(offer);
        };

        var compareOffer  = function (offer1, offer2) {
            return JSON.stringify(offer1.selected) === JSON.stringify(offer2.selected);
        };
        var compareOffers = function (offer) {
            var i = -1;
            _.some(offers, function (o, index) {
                if (compareOffer(o, offer)) {
                    i = index;
                    return true;
                }
            });
            return i;
        };

        var setSelectedOffer   = function (offer, persist) {
            persist = persist === undefined ? true : persist;

            $rootScope.$broadcast(SB_EVENT.changeOffer, offer);
            selectedOffer = offer;

            if (persist) {
                sbSessionPersistence.setItem(SB_PERSISTENCE.selectedOffer, selectedOffer);
            }
        };
        var getSelectedOffer   = function () {
            return selectedOffer;
        };
        var resetSelectedOffer = function () {
            $rootScope.$broadcast(SB_EVENT.changeOffer, null);
            selectedOffer = null;
            sbSessionPersistence.removeItem(SB_PERSISTENCE.selectedOffer);
        };

        var getOfferCount = function () {
            return offers.length;
        };

        /**
         * Delete the current offer in the wish list and broadcast deleteOffer event
         * @param {object} offer - Object containing current offer details.
         */
        var deleteOffer = function (offer) {
            $rootScope.$broadcast(SB_EVENT.deleteOffer, offer);

            if (selectedOffer && compareOffer(offer, selectedOffer)) {
                resetSelectedOffer();
            }
            offers.splice(offers.indexOf(offer), 1);

            sbSessionPersistence.setItem(SB_PERSISTENCE.offers, offers);
        };

        var addOffer = function (offer) {

            offers[0] = offer;
            sbSessionPersistence.setItem(SB_PERSISTENCE.offers, offers);

            $rootScope.$broadcast(SB_EVENT.addOffer, offer);
        };

        /**
         * Saves the offer in the wish list and broadcast events
         * @param {object} offer - Object containing offer details.
         */
        var _saveOffer = function (offer) {
            // If there is already an offer, it will be replaced with a new one;
            // therefore, broadcast the deleteOffer event with the previous offer as arg
            if(offers[0]) {
                $rootScope.$broadcast(SB_EVENT.deleteOffer, offers[0]);
            }

            // Assign the new offer and broadcast the saveOffer event with the new offer as arg
            offers[0] = offer;
            sbSessionPersistence.setItem(SB_PERSISTENCE.offers, offers);

            sbResources.save(sbOfferParser.createOfferXML(offers)).then(
                function success(data) {
                    try {
                        offer.offerID = data.response.results.result.offerID;
                    } catch (e) {
                        offer.offerID = undefined;
                        console.warn('no offerID available :', e);
                    }
                    offer.saveResponse = data;
                    offers[0] = offer;
                    sbSessionPersistence.setItem(SB_PERSISTENCE.offers, offers);
                    $rootScope.$broadcast(SB_EVENT.saveOffer, offer);
                    resetSelectedOffer();
                },
                function error(reason) {
                    console.error('Problems saving:', reason);
                }
            );

        };

        var saveOffer = function () {

            if (!sbOfferParser.offerAvailable()) {
                return;
            }

            sbOfferParser.createOfferObject().then(
                function (offer) {

                    if (busyRetrieving) {
                        var listener = $rootScope.$on(SB_EVENT.offersRetrieved, function () {
                            _saveOffer(offer);
                            listener();
                        });
                    } else {
                        _saveOffer(offer);
                    }
                }
            );
        };

        var retrieveOffers = function () {

            busyRetrieving = true;
            sbSessionPersistence.setItem(SB_PERSISTENCE.busyRetrieving, true);

            sbResources.retrieve(sbOfferParser.createRetrieveXML())
                .then(function (data) {
                    var xml = x2js.xml_str2json(data);
                    if (xml) {
                        sbOfferParser.createOfferObject(xml, addOffer).then(
                            function success() {
                                $rootScope.$broadcast(SB_EVENT.offersRetrieved);
                                busyRetrieving = false;
                                sbSessionPersistence.setItem(SB_PERSISTENCE.busyRetrieving, false);
                            }, function error(e) {
                                $rootScope.$broadcast(SB_EVENT.offersRetrieved);
                                console.debug(e);
                                busyRetrieving = false;
                                sbSessionPersistence.setItem(SB_PERSISTENCE.busyRetrieving, false);
                            });
                    }
                });
        };

        function initDesktop() {
            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.busyRetrieving)) {
                busyRetrieving = sbSessionPersistence.getItem(SB_PERSISTENCE.busyRetrieving);
            }


            if (busyRetrieving) {
                retrieveOffers();
            }

            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.offers)) {
                offers = sbSessionPersistence.getItem(SB_PERSISTENCE.offers);
            }
            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.selectedOffer)) {
                setSelectedOffer(sbSessionPersistence.getItem(SB_PERSISTENCE.selectedOffer), false);
            }
        }

        if (SB_ENV.isDesktop) {
            initDesktop();
        }

        return {
            offerAvailable: sbOfferParser.offerAvailable,
            getOfferCount: getOfferCount,
            getMaxOfferCount: function () {
                return maxOffers;
            },
            saveOffer: saveOffer,
            retrieveOffers: retrieveOffers,
            compareOffers: compareOffers,
            deleteOffer: deleteOffer,
            offers: offers,
            setSelectedOffer: setSelectedOffer,
            getSelectedOffer: getSelectedOffer,
            hasSelectedOffer: function () {
                return !!getSelectedOffer();
            },
            resetSelectedOffer: resetSelectedOffer,
            getPdpParams: sbOfferParser.pdpParams,
            navigateToOffer: navigateToOffer,
            orderOffer: orderOffer
        };
    }

})();
