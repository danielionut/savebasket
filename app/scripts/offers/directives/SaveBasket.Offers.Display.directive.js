(function () {

    'use strict';

    angular.module('SaveBasket.Offers')
        .directive('sbOfferDisplay', SaveBasketOfferDisplayDirective);

    function SaveBasketOfferDisplayDirective(SB_PATH) {
        return {

            restrict: 'E',
            scope: {
                offer: '=',
                path: '='
            },
            controller: SaveBasketOfferDisplayController,
            templateUrl: SB_PATH.templates + 'offers/templates/sbOfferDisplay.tpl.html'

        };
    }

    function SaveBasketOfferDisplayController($scope, SaveBasket) {
        $scope.deleteOffer = function(offer) {
            SaveBasket.deleteOffer(offer);
        };
        $scope.orderOffer  = function(offer) {
            SaveBasket.orderOffer(offer);
        };
        $scope.changeOffer = function(offer) {
            SaveBasket.changeOffer(offer);
        };
    }



})();