(function () {

    'use strict';

    angular.module('SaveBasket.Offers.Parser', [
            'Tmobile.Services.Product'
        ])
        .factory('sbOfferParser', SaveBasketOfferParser);

    function SaveBasketOfferParser(x2js, tmReceipt, tmSelectedProduct, tmLinks, tmDescription, $state, sbUser, sbLogic, sbResources, Params, tmPdp, tmRatePlanPricing, tmPricing, tmRatePlan, SB_PATH, $q, TM_RATE_PLAN, $rootScope, $timeout, SB_EVENT) {

        var debug = true;

        debug = function (what, description) {
            if (debug) {
                console.log(description + ' :', what);
            }
        };

        var createConfig    = function (products) {
            return _.map(_.filter(products, function (product) {
                return product.type !== 'HCL' && product.type !== 'color' && product.type !== 'UPR';
            }), function (product) {
                return product.products[0].id;
            }).join();
        };
        var createPdpParams = function (offer) {
            return {
                id: offer.id,
                config: offer.origin === 'app' ? createConfig(offer.selected) : offer.pdpConfig,
                description: tmDescription.getUrlDescription(offer.raw)
            };
        };

        var findAllSelected = function (links, arr) {
            arr = arr || [];
            _.each(links, function (l) {
                // cloud !!
                l.products = _.filter(l.products, function(p) {
                    return p.selected;
                });
                if (l.hasOwnProperty('selectedPlan') && l.selected) {
                        arr.push(l);
                } else if (l.products[0]) {
                    if ((!l.hasOwnProperty('selected') && l.products[0].selected) ||
                        (l.hasOwnProperty('selected') && l.selected === true)) {
                        arr.push(l);
                    }
                    if (l.products[0].links) {
                        arr = findAllSelected(l.products[0].links, arr);
                    }
                }
            });

            return arr;
        };
        var createExtra     = function (products, type) {
            try {
                return tmLinks.createExtraObject(tmLinks.getObjectWithType(products, 'type', type).products[0].extra);
            } catch (e) {

            }
        };
        var getExtra        = function (extras, fieldName) {
            var result = _.filter(extras, function (extra) {
                return extra.fieldName === fieldName;
            });
            return result.length ? result[0].value : '';
        };
        var getAttribute    = function (product, id) {
            var p = _.filter(product.products[0].attributes, function (extra) {
                return extra.id === id;
            });
            if (p[0] && p[0].value) {
                return p[0].value;
            }
        };

        var navigateToOffer = function (offer) {
            sbLogic.resetRatePlan();
            var toState = $state.current.name.indexOf('.pdp.') !== -1 ? $state.current.name : $state.current.data.nextState;
            $state.transitionTo(toState, createPdpParams(offer), {reload: true, inherit: false, notify: true});
        };

        var orderOffer = function (offer) {

            if (offer.origin === 'app') {
                tmSelectedProduct.setProduct(offer.selectedProductObject);
                tmReceipt.updateReceipt();
                $timeout(function(){
                    $state.go('home.ca.checkout.step-1-budget');
                    var listener = $rootScope.$on('$stateChangeSuccess', function() {
                        $rootScope.$broadcast(SB_EVENT.closeModal);
                        listener();
                    });

                });
            } else {
                var listener = $rootScope.$on(TM_RATE_PLAN.updatePrice, function () {
                    $timeout(function () {
                        $state.go('home.ca.checkout.step-1-budget');
                        var listener2 = $rootScope.$on('$stateChangeSuccess', function() {
                            $rootScope.$broadcast(SB_EVENT.closeModal);
                            listener2();
                        });
                        listener();
                    }, 800);

                });
                navigateToOffer(offer);
            }
        };

        var createOfferXML = function (offers) {
            var XML        = {
                    basket: {
                        customercode: sbUser.getInfo('code').substr(0, 10),
                        MSISDN: sbUser.getInfo('phone'),
                        emailAddress: sbUser.getInfo('email'),
                        salesAgentCode: '0000099151',
                        salesChannel: 'ESLS'
                    }
                },
                offerArray = [];

            _.forEach(offers, function (offer, i) {

                var result = _.chain(offer.selected)
                    .filter(function (product) {
                        return product.type !== 'color' && product.type !== 'HCL' && product.type !== 'SIM';
                    })
                    .map(function (product) {
                        var obj         = {};
                        obj.art_bbid = getExtra(product.products[0].extra, 'externalId');
                        obj.art_custom1 = product.type === 'AB' ? 'A' : 'O';
                        if (product.type === 'AB') {

                            obj.ab_agexport = 'RATEPLAN';
                        } else {
                            obj.ab_agexport = 'SERVICE';
                        }
                        var term     = getAttribute(product, 'abotermy') * 12;
                        term     = isNaN(term) ? '' : term;
                        obj.abotermy = term;
                        return obj;
                    }).value();

                var device = offers[i].raw;

                var obj         = {};
                obj.art_bbid    = getExtra(device.extra, 'externalId');
                obj.ab_agexport = getExtra(device.extra, 'art_issimonly') === 'TRUE' ? 'SIM' : 'HANDSET';
                obj.art_custom1 = '';
                obj.abotermy    = '';
                result.unshift(obj);

                var ol = {offerline: result};
                offerArray.push(ol);
            });

            XML.basket.offers = {offer: offerArray};

            return x2js.json2xml_str(XML);
        };

        var createOfferObject = function (offerXML, addOfferFunc) {

            var offer, receipt;

            var defer = $q.defer();

            if (!offerXML) {

                var o = {};


                offer                   = window.deepCopy(tmSelectedProduct.getProduct());
                o.selectedProductObject = window.deepCopy(offer);

                receipt = window.deepCopy(tmReceipt.getReceipt());

                o.receipt = receipt;
                o.origin  = 'app';
                o.id      = offer.id;
                o.title   = offer.supplier + ' ' + offer.name;

                o.image = offer.image.data;
                if (o.image.indexOf('http') === -1) {
                    o.image = SB_PATH.url + o.image;
                }

                o.selected  = findAllSelected(offer.links, []);
                o.minutes   = createExtra(o.selected, 'MIN');
                o.data      = createExtra(o.selected, 'DAT');
                o.rateplan  = createExtra(o.selected, 'AB');
                o.insurance = _.filter(o.selected, function (p) {
                    return p.type === 'INS';
                });
                o.raw       = offer;
                defer.resolve(o);

            }
            else if (offerXML.basket.response.result.resultCode !== '0') {

                defer.reject('No result from TMNL');

            }
            else if (offerXML.basket) {

                var promises = [];

                _.forEach(offerXML.basket.offers, function (offer) {

                    var o = {};

                    o.id = findOfferLineWithField(offer, 'ab_agexport', 'HANDSET', 'art_id');
                    if (!o.id) {
                        o.id = findOfferLineWithField(offer, 'ab_agexport', 'SIM', 'art_id');
                    }

                    if (!o.id) {
                        o.error = {
                            id: 1,
                            internalMessage: 'No tangible product found'
                        };
                        addOfferFunc(o);
                        return;
                    }

                    o.configObj = _.chain(offer.offerline)
                        .filter(function (offerline) {
                            return offerline.ab_agexport === 'SERVICE' || offerline.ab_agexport === 'RATEPLAN';
                        })
                        .map(function (offerline) {
                            return {
                                id: offerline.art_id,
                                available: offerline.available === 'True',
                                offerline: offerline
                            };
                        }).value();

                    o.config    = _.map(_.filter(o.configObj, 'available'), 'id').join(',');
                    o.pdpConfig = _.map(_.filter(o.configObj, 'available'), function (conf) {
                        return conf.offerline.base_article ? conf.offerline.base_article : conf.id;
                    }).join(',');

                    promises.push(sbResources.getFullProduct({
                        id: o.id,
                        config: o.config
                    }).then(function (response) {
                        var product = response[0];

                        if (!product) {

                            o.error = {
                                id: 2,
                                internalMessage: 'No product found'
                            };
                            addOfferFunc(o);
                            return;

                        }

                        try {

                            tmRatePlan.setZeroPriceItems(product);
                            o.image = product.image.data;
                            if (o.image.indexOf('http') === -1) {
                                o.image = SB_PATH.url + o.image;
                            }
                            o.offerID = offer.offerID;
                            o.retrieveRsponse = offer;
                            o.raw               = product;
                            o.title             = o.raw.supplier + ' ' + o.raw.name;
                            o.origin            = 'xml';
                            o.rateplan          = findOfferLineWithField(offer, 'ab_agexport', 'RATEPLAN', 'art_id');
                            o.rateplanAvailable = findOfferLineWithField(offer, 'ab_agexport', 'RATEPLAN', 'available') === 'True';
                            o.addOns            = [];

                            var ab         = tmLinks.getObjectWithType(product.links, 'type', 'AB');
                            var ins        = tmLinks.getObjectWithType(product.links, 'type', 'INS');
                            //var selectedAb = _.find(ab.products, function (rateplan) {
                            //    return rateplan.id === o.rateplan;
                            //});

                            o.ab = tmLinks.setDefaultSelection(ab);
                            //tmLinks.setDefaultSelection(o.ab);

                            _.each(o.ab.links, function (link) {
                                //console.log(link);
                                tmLinks.setDefaultSelection(link, null, o.config);
                            });

                            var copyProduct = window.deepCopy(tmSelectedProduct.getProduct());

                            var cp = window.deepCopy(tmSelectedProduct.setDefaults(window.deepCopy(product), null, o.config));
                            _.some(cp.links, function (link) {
                                if (link.type === 'AB' && link.products && link.products[0]) {
                                    link.products[0] = o.ab;
                                }
                            });

                            o.selectedProductObject = cp;
                            o.selected  = findAllSelected(cp.links, []);

                            tmSelectedProduct.setProduct(copyProduct);

                            var cpAb = tmLinks.getObjectWithType(cp.links, 'type', 'AB');

                            _.each(cpAb.products[0].links, function (link) {
                                link.products = _.filter(link.products, 'selected');
                            });

                            var pricing = tmPdp.createPricingObject(cp, tmRatePlanPricing.getPricing(o.ab));

                            _.map(pricing, function (price) {
                                if (isNaN(price)) {
                                    return null;
                                }
                                else {
                                    return price;
                                }
                            });

                            o.rateplan           = tmLinks.createInfoObject(o.ab);
                            o.rateplan.duration  = findOfferLineWithField(offer, 'ab_agexport', 'RATEPLAN', 'abotermy');
                            o.rateplan.available = o.rateplanAvailable;

                            tmPricing.calcPricing(pricing).then(function (p) {
                                o.receipt                            = {};
                                o.receipt.finalPricing               = p;
                                o.selectedProductObject.finalPricing = p;
                                o.selectedProductObject.finalPricing.subscriptionDuration = o.rateplan.duration;
                            });

                            _.each(o.configObj, function (conf) {

                                var art = conf.id;

                                var match = _.filter(ins.products, function (p) {
                                    return p.id === art;
                                });

                                if (match.length) {
                                    o.insurance            = match[0];
                                    o.insurance.available  = conf.available;
                                    o.insurance.monthlyFee = parseFloat(getExtra(o.insurance.extra, 'monthlyFee'));
                                }
                                _.each(o.ab.links, function (link) {

                                    var match = _.filter(link.products, function (prod) {
                                        return prod.id === art;
                                    });

                                    if (match.length) {
                                        o[link.type] = match[0];
                                        if (link.type.indexOf('X') === 0 || link.type === 'SDP') {
                                            o.addOns.push({
                                                name: match[0].description,
                                                available: conf.available
                                            });
                                        }
                                    }

                                });
                            });
                            if (o.DAT) {
                                o.data = tmLinks.createExtraObject(o.DAT.extra);
                            }
                            if (o.MIN) {
                                o.minutes = tmLinks.createExtraObject(o.MIN.extra);
                            }

                            //console.log('offer:', o);

                            addOfferFunc(o);

                        } catch (e) {
                            o.error = {
                                id: 3,
                                internalMessage: 'Something went wrong parsing the offer!',
                                errorMessage: e
                            };
                            addOfferFunc(o);

                        }
                    }));
                });
                $q.all(promises).then(function() {
                    defer.resolve();
                });

            }
            else {
                defer.reject('Something went wrong parsing the offers');
            }
            return defer.promise;
        };

        var createRetrieveXML = function () {
            var XML = {
                basket: {
                    customercode: sbUser.getInfo('code').substr(0, 10),
                    MSISDN: sbUser.getInfo('phone'),
                    cust_fc: sbUser.getInfo('value'),
                    cust_email: sbUser.getInfo('email')
                }
            };
            return x2js.json2xml_str(XML);
        };

        /// function to pare after a retrieve //
        function findOfferLineWithField(offer, field, value, f) {

            var result = _.find(offer.offerline, function (offerLine) {
                return offerLine[field] === value;
            });
            if (result && f) {
                return result[f];
            } else {
                return result;
            }

        }

        var offerAvailable = function () {
            return !!tmSelectedProduct.getProduct();
        };

        function getSelectedProductObject() {

            if (offerAvailable()) {
                var p      = tmSelectedProduct.getProduct();
                var config = createConfig(findAllSelected(p.links));

                //var c = createConfig(findAllSelected(p.links));

                var obj = {
                    id: p.id,
                    description: tmDescription.getUrlDescription(p)
                };
                if (config) {
                    obj.config = config;
                }

                return obj;
            }

            return false;

        }

        return {
            offerAvailable: offerAvailable,
            createOfferObject: createOfferObject,
            pdpParams: createPdpParams,
            navigateToOffer: navigateToOffer,
            orderOffer: orderOffer,
            createOfferXML: createOfferXML,
            createRetrieveXML: createRetrieveXML,
            getSelectedProductObject: getSelectedProductObject
        };

    }
})();