(function () {

    'use strict';

    angular.module('SaveBasket.SalesChannel', [
        'SaveBasket.Resources'
    ]);

})();