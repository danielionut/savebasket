(function () {

    'use strict';


    angular.module('SaveBasket.User')
        .factory('sbUserLogin', SaveBasketUserLoginService);

    /**
     * @ngdocs service
     * @name sbUserLogin
     * @description Handles the login for a ECA customer. The login for a ECR customer is handled the normal way by the application
     * If a cryptoticket is available on ECA, it means that the customer is redirected back from the Portals verification.
     */
    function SaveBasketUserLoginService(SB_PATH, SB_EVENT, $rootScope, sbOfferParser, SB_PORTALS_LOGIN) {

        var currentCommand = '';
        $rootScope.$on(SB_EVENT.currentCommand, function(event, command) {
            currentCommand = command;
        });

        function login() {
            window.location = SB_PORTALS_LOGIN.url + generateRedirectUrl();
        }

        function generateRedirectUrl() {
            var url = window.location.origin + window.location.pathname,
                obj = sbOfferParser.getSelectedProductObject();

            if (!obj) {
                return encodeURIComponent(url);

            } else {
                url = window.location.origin + '/' + SB_PATH.channel + '/RAPRD/'+ obj.description + '/' + obj.id + '.html?xart_id=' + obj.config;

                if (currentCommand === 'save') {
                    url += '&savebasket=true';
                }

                return encodeURIComponent(url);
            }

        }
        $rootScope.$on(SB_EVENT.validateUser, function() {
            login();
        });


        return {
            login: login
        };

    }

})();