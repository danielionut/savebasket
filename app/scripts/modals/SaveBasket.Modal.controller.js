(function() {

    'use strict';

    angular.module('SaveBasket')
        .controller('sbDefaultModalController', SaveBasketDefaultModalController)
        .controller('sbOffersModalController', SaveBasketOffersModalController);


    //SaveBasketDefaultModalController.$inject = ['$rootScope', '$scope', '$modalInstance', 'SB_EVENT'];
    function SaveBasketDefaultModalController($rootScope, $scope, $modalInstance, SB_EVENT, sbUser) {

        var close = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.user = sbUser.get();

        $scope.close = close;

        $rootScope.$on('$stateChangeStart', function () {
            $modalInstance.dismiss('cancel');
        });
        $rootScope.$on(SB_EVENT.modalClose, function () {
            $modalInstance.dismiss('cancel');
        });
    }

    //SaveBasketOffersModalController.$inject = ['$rootScope', '$scope', '$modalInstance','SaveBasket', 'SB_EVENT'];
    function SaveBasketOffersModalController($rootScope, $scope, $modalInstance, SaveBasket, SB_EVENT) {

        var close = function () {
            $modalInstance.dismiss('cancel');
        };

        var login = function() {
            $rootScope.$broadcast(SB_EVENT.validateUser);
        };


        $scope.close = close;
        $scope.login = login;
        $scope.offerAvailable = SaveBasket.offerAvailable();

        $rootScope.$on('$stateChangeSuccess', function () {
            $modalInstance.dismiss('cancel');
        });
        $rootScope.$on(SB_EVENT.modalClose, function () {
            $modalInstance.dismiss('cancel');
        });

        $scope.offers = SaveBasket.offers;


    }

})();