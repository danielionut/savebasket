(function () {
    'use strict';

    angular
        .module('SaveBasket.Modal')
        .factory('sbModal', SaveBasketModalService);

    function SaveBasketModalService(SaveBasket, $templateCache, $modal, $rootScope, SB_EVENT, sbUser) {

        var modals = {
            info: {
                template: $templateCache.get('modals/templates/infoOverlay.tpl.html'),
                controller: 'sbOffersModalController'
            },
            offers: {
                template: $templateCache.get('modals/templates/offerModal.tpl.html'),
                controller: 'sbOffersModalController'
            },
            emptyBasket: {
                template: $templateCache.get('modals/templates/emptyBasketModal.tpl.html'),
                controller: 'sbOffersModalController'
            },
            replaceOffer: {
                template: $templateCache.get('modals/templates/replaceOverlay.tpl.html'),
                controller: 'sbOffersModalController'
            },
            login: {
                template: $templateCache.get('modals/templates/loginOverlay.tpl.html'),
                controller: 'sbOffersModalController'
            },
            noOffer: {
                template: $templateCache.get('modals/templates/noOfferOverlay.tpl.html'),
                controller: 'sbOffersModalController'
            }
        };
        var closeAllModals = function() {
            $rootScope.$broadcast(SB_EVENT.modalClose);
        };

        var openBasket = function(command) {

            closeAllModals();

            if (!sbUser.get().validation) {
                SaveBasket.login();
            }
            else if (!SaveBasket.offerAvailable() && (command === 'replace' || command === 'save')) {
                $modal.open(modals.noOffer);
            }
            else if (command === 'replace') {
                SaveBasket.saveOffer();
                $modal.open(modals.offers);
            }
            else if (command === 'save' && SaveBasket.hasSelectedOffer()) {
                SaveBasket.saveOffer();
                $modal.open(modals.offers);
            }
            else if (command === 'save' && SaveBasket.offerCount() >= SaveBasket.maxOffers) {
                $modal.open(modals.replaceOffer);
            }
            else if (command === 'save' && SaveBasket.firstSave) {
                SaveBasket.firstSave = false;
                SaveBasket.saveOffer();
                $modal.open(modals.info);
            } else if (command === 'save') {
                SaveBasket.saveOffer();
                $modal.open(modals.offers);
            } else if (SaveBasket.offerCount()) {
                $modal.open(modals.offers);
            } else {
                $modal.open(modals.emptyBasket);
            }
        };

        $rootScope.$on(SB_EVENT.openBasket, function(event, command) {
            openBasket(command);
        });

        return {
            openBasket: openBasket
        };
    }
})
();
