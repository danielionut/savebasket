(function () {
    'use strict';

    angular.module('SaveBasket.DataLayer')
        .factory('sbDataLayer', SaveBasketDataLayerService);

    function SaveBasketDataLayerService($rootScope, sbDataLayerParser, SB_EVENT, $window) {

        var addOffer = function (offer) {
            sbDataLayerParser.create(offer).then(
                function success(data) {
                    offer.dataLayer = data;
                    pushToDataLayer(data, 'add', offer);
                },
                function error(reason) {
                    console.error('Unable to load dataLayer: ', reason);
                }
            );
        };

        var deleteOffer = function (offer) {
            if (offer && offer.dataLayer) {
                pushToDataLayer(offer.dataLayer, 'remove');
                return;
            }

            sbDataLayerParser.create(offer).then(
                function success(data) {
                    pushToDataLayer(data, 'remove', offer);
                },
                function error(reason) {
                    console.error('Unable to load dataLayer: ', reason);
                }
            );
        };

        var pushToDataLayer = function (obj, action, offer) {
            var layer;
            if (action === 'add') {
                layer = {
                    event: 'addToCart',
                    Shop: {
                        ProductsInWishlist: obj
                    }
                };
            } else if (action === 'remove') {
                layer = {
                    event: 'removeFromCart',
                    Shop: {
                        ProductsRemoveFromWishlist: obj
                    }
                };
            }
            $window.dataLayer = $window.dataLayer || [];
            $window.dataLayer.push(layer);

            if (offer) {
                offer.dataLayer = layer.Shop.ProductsInWishlist;
            }

            $rootScope.$broadcast('dataLayerEvent', {action: action, layer: layer, offer: offer});
        };

        $rootScope.$on(SB_EVENT.saveOffer, function (event, offer) {
            addOffer(offer);
        });

        $rootScope.$on(SB_EVENT.deleteOffer, function (event, offer) {
            deleteOffer(offer);
        });

    }
})();
