
(function () {
    'use strict';

    /**
     * @ngdoc overview
     * @name SaveBasket
     * @description The main module for SaveBasket
     */

    angular
        .module('SaveBasket', [

            'ngAnimate',
            'xml',

            /* Mobile Only */
            'Tmobile.Services.SelectedProduct',
            'Tmobile.Services.Description',
            'Tmobile.Services.Channel',

            'SaveBasket.Templates',

            'SaveBasket.User',
            'SaveBasket.Offers',
            'SaveBasket.Modal',
            'SaveBasket.Trigger',
            'SaveBasket.Constants',
            'SaveBasket.Logic',
            'SaveBasket.Resources',
            'SaveBasket.Session.Persistence',
            'SaveBasket.DataLayer'


        ])
        .run(function($rootScope, TM_AUTH_EVENTS, sbUser, SaveBasket, SB_EVENT) {
            $rootScope.$on(TM_AUTH_EVENTS.authenticated, function ($event, customer) {
                sbUser.set(customer);
            });
            var userValidatedListener = $rootScope.$on(SB_EVENT.userValidated, function (event, user, doRetrieve) {
                doRetrieve = doRetrieve === undefined ? true : doRetrieve;
                if (doRetrieve) {
                    SaveBasket.retrieveAfterUserValidated();
                    userValidatedListener();
                }
            });
        });
})();
