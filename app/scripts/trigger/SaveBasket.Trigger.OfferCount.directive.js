(function () {
    'use strict';

    angular.module('SaveBasket.Trigger')
        .directive('sbOfferCount', saveBasketOfferCountDirective);

    function saveBasketOfferCountDirective(SaveBasket) {
        return {
            restrict: 'A',
            link: function(scope, element) {

                setAttribute(SaveBasket.offerCount());

                scope.$watch(function() {
                    return SaveBasket.offerCount();
                }, function(val) {
                    setAttribute(val);
                });

                function setAttribute(nval, oval) {
                    if (nval !== oval) {
                        if (nval) {
                            element.attr('sb-offer-count-value', nval);
                        } else {
                            element.removeAttr('sb-offer-count-value');
                        }

                    }
                }

            }
        };
    }

})();