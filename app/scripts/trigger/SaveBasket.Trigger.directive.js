(function () {

    'use strict';

    angular.module('SaveBasket.Trigger')
        .directive('sbTrigger', saveBasketTriggerDirective);

    function saveBasketTriggerDirective(SaveBasket, sbModal, $rootScope, SB_EVENT) {
        return {
            restrict: 'EA',
            scope: {
                command: '@sbTrigger',
                openBasket: '=openBasket',
                addText: '@',
                changeText: '@'
            },
            controller: function($rootScope) {
                //ToDo - find a better solution! - Used for configurable text on trigger link.
                $rootScope.addOffer = !SaveBasket.hasSelectedOffer();
                $rootScope.changeOffer = SaveBasket.hasSelectedOffer();
            },
            link: function (scope, element) {

                element.removeClass('hidden');

                if (SaveBasket.hasSelectedOffer()) {
                    setText(scope.changeText);
                } else {
                    setText(scope.addText);
                }

                $rootScope.$on(SB_EVENT.changeOffer, function(event, offer) {

                    if (offer) {
                        setText(scope.changeText);
                    } else {
                        setText(scope.addText);
                    }
                });

                $rootScope.$on(SB_EVENT.showBasket, function(event, show) {
                    if (show) {
                        element.removeClass('hidden');
                    } else {
                        element.addClass('hidden');
                    }
                });

                element.on('click', function () {

                    $rootScope.$broadcast(SB_EVENT.currentCommand, scope.command);

                    if (typeof scope.openBasket === 'undefined' || scope.openBasket) {
                        sbModal.openBasket(scope.command);
                    }

                });

                function setText(text) {
                    if (text) {
                        element.find('.sb-trigger-text').text(text);
                    }

                }
            }
        };
    }

})();