(function () {

    'use strict';

    angular.module('SaveBasket.Session.Persistence', ['ngCookies']).factory('sbSessionPersistence', SaveBasketSessionPersistenceService);
    /**
     * @ngdoc service
     * @name sbSessionPersistence
     *
     * @description
     * Temporarily persists objects in a value-key map
     */
    function SaveBasketSessionPersistenceService(SB_ENV) {
        var cookieName = 'sbSessionStorage',
            hashMap    = [];

        function init() {
            hashMap = JSON.parse(sessionStorage.getItem(cookieName));
            if (!angular.isArray(hashMap)) {
                hashMap = [];
            }
        }

        function clearSession() {
            sessionStorage.setItem(cookieName, '');
        }

        function update() {
            sessionStorage.setItem(cookieName, JSON.stringify (hashMap));
        }

        if (SB_ENV.isDesktop) {
            init();
            window.onbeforeunload = function () {
                //sessionStorage.setItem(cookieName, JSON.stringify (hashMap));
            };
        }

        function getItem(key) {
            if (SB_ENV.isMobile) {
                return false;
            }
            var item,
                result,
                i;
            for (i = 0; i < hashMap.length; i++) {
                item = hashMap[i];
                if (item.k === key) {
                    result = item.v;
                }
            }
            return result;
        }

        function setItem(key, data) {
            if (SB_ENV.isMobile) {
                return;
            }
            removeItem(key);
            hashMap.push({k: key, v: data});
            update();
        }

        function removeItem(key) {
            if (SB_ENV.isMobile) {
                return;
            }
            var item,
                i,
                _hashMap = [];
            for (i = 0; i < hashMap.length; i++) {
                item = hashMap[i];
                if (item.k !== key) {
                    _hashMap.push(item);
                }
            }
            hashMap = _hashMap;
            update();
        }

        function getAllItems() {
            if (SB_ENV.isMobile) {
                return false;
            }
            var item,
                i,
                result = [];
            for (i = 0; i < hashMap.length; i++) {
                item = hashMap[i];
                result.push(item.v);
            }
            return result;
        }

        function getAllItemsWithKeys() {
            if (SB_ENV.isMobile) {
                return false;
            }
            return hashMap;
        }

        function clearItems() {
            if (SB_ENV.isMobile) {
                return false;
            }
            hashMap = [];
            update();
        }

        function itemExists(key) {
            if (SB_ENV.isMobile) {
                return false;
            }
            var item,
                i,
                result = false;
            for (i = 0; i < hashMap.length; i++) {
                item = hashMap[i];
                if (item.k === key) {
                    result = true;
                    break;
                }
            }
            return result;
        }

        return {
            setItem: setItem,
            getItem: getItem,
            removeItem: removeItem,
            getAllItems: getAllItems,
            getAllItemsWithKeys: getAllItemsWithKeys,
            clearItems: clearItems,
            itemExists: itemExists,
            clear: clearSession
        };
    }
})();