(function () {

    'use strict';

    /**
     * @module SaveBasket.Resources
     * @description Services for retrieving and saving offers and
     * getting a product json for the product in a offer
     */



    angular.module('SaveBasket.Resources')
        .factory('sbResources', SaveBasketResourcesService);

    function SaveBasketResourcesService($http, SB_PATH, $q, ProductResource, x2js) {

        /**
         * Saves an offer
         *
         * @param offerXML
         * @returns {*|r.promise|promise|m.promise|{then, catch, finally}}
         */
        var save = function (offerXML) {

            var defer = $q.defer();

            $http({
                method: 'post',
                data: offerXML,
                headers: {
                    'Content-Type': 'application/XML'
                },
                url: SB_PATH.url + 'service/basket/save'
            }).then(
                function succes(response) {
                    defer.resolve(x2js.xml_str2json(response.data));
                },
                function error(reason) {
                    defer.reject(reason);
                }
            );

            return defer.promise;

        };

        var retrieve = function (xml) {

            var defer = $q.defer();

            $http({
                method: 'post',
                data: xml,
                headers: {
                    'Content-Type': 'application/XML'
                },
                url: SB_PATH.url + 'service/basket/get'
            }).then(
                function succes(response) {
                    defer.resolve(response.data);
                },
                function error(reason) {
                    defer.reject(reason);
                }
            );

            return defer.promise;
        };

        /** Retrieve the full json object for a given product **/
        var getFullProduct = function (params) {

            return ProductResource.get(params).$promise;

        };

        return {
            save: save,
            retrieve: retrieve,
            getFullProduct: getFullProduct
        };

    }

})();