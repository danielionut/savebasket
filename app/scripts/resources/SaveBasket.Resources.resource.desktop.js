(function () {

    'use strict';

    /**
     * @module SaveBasket.Resources
     * @description Services for retrieving and saving offers and
     * getting a product json for the product in a offer
     */



    angular.module('SaveBasket.Resources')
        .factory('sbResources', SaveBasketResourcesService);

    function SaveBasketResourcesService($http, SB_PATH, $q, sbFormInfo, SB_MAMBO_USER, x2js) {

        /**
         * Saves an offer
         *
         * @param offerXML
         * @returns {*|r.promise|promise|m.promise|{then, catch, finally}}
         */
        var save = function (offerXML) {

            var defer = $q.defer();

            $http({
                method: 'post',
                data: offerXML,
                headers: {
                    'Content-Type': 'text/xml'
                },
                url: '/' + SB_PATH.channel + '/mcsgoxml.p?methodname=xtnsvbsk&pmu_id=' + SB_MAMBO_USER.save + '&pmu_pwd=' + SB_MAMBO_USER.password
            }).then(
                function succes(response) {
                    defer.resolve(x2js.xml_str2json(response.data));
                },
                function error(reason) {
                    defer.reject(reason);
                }
            );

            return defer.promise;

        };

        var retrieve = function (xml) {

            var defer = $q.defer();

            $http({
                method: 'post',
                data: xml,
                headers: {
                    'Content-Type': 'text/xml'
                },
                url: '/' + SB_PATH.channel + '/mcsgoxml.p?methodname=ttnxabsk&pmu_id=' + SB_MAMBO_USER.retrieve + '&pmu_pwd=' + SB_MAMBO_USER.password
            }).then(
                function succes(response) {
                    defer.resolve(response.data);
                },
                function error(reason) {
                    defer.reject(reason);
                }
            );

            return defer.promise;
        };

        /** Retrieve the full json object for a given product **/
        var getFullProduct = function (params) {

            var defer = $q.defer();
           $http({
               method: 'get',
               url: '/' + SB_PATH.channel + '/mcsmambo.p',
               params:  angular.extend({
                   'M5NextUrl': 'MDORJSON',
                   'M5CurrUrl' : 'RAPRD',
                   'art_id' : sbFormInfo.get('art_id'),
                   'xart_id' : sbFormInfo.get('xart_id'),
                   'objectType' : 'RPC',
                   'M5WebChoice' : 'CMT_91',
                   'init_abo_id' : sbFormInfo.get('init_abo_id')
               }, params)
           }).then(
               function succes(response) {
                   defer.resolve(response.data);
               },
               function error(reason) {
                   defer.reject(reason);
               }
           );

           return defer.promise;

        };

        function getUser() {
            var defer = $q.defer();

            $http({
                    method: 'get',
                    url: '/' + SB_PATH.channel + '/mcsmambo.p',
                    params: {
                        'objecttype': 'rpc',
                        'm5NextUrl': 'mdorcust',
                        'compatible': true
                    }
                }).then(
                function success(response) {
                    defer.resolve(response.data);
                },
                function error(reason) {
                    defer.reject(reason);
                });

            return defer.promise;
        }

        function getSalesAgent() {
            var defer = $q.defer();

            $http({
                method: 'get',
                url: '/' + SB_PATH.channel + '/mcsmambo.p',
                params: {
                    'objecttype': 'rpc',
                    'm5NextUrl': 'mdoagent',
                    'compatible': true
                }
            }).then(
                function success(response) {
                    var data = response.data;
                    if (data.mdoagent && data.mdoagent.tt_dealercode && data.mdoagent.tt_dealercode[0]) {
                        defer.resolve(data.mdoagent.tt_dealercode[0]);
                    } else {
                        defer.reject('no SalesAgent object present');
                    }

                },
                function error(reason) {
                    defer.reject(reason);
                });

            return defer.promise;
        }
        function getDataLayer(options) {
            var defer = $q.defer(),
                params = {
                    'objectType': 'rpc',
                    'm5NextUrl': 'mdodlpix',
                    'compatible': true,
                    'Pixelreload': true
                };

            params = angular.extend(params, options);

            $http({
                method: 'get',
                url: '/' + SB_PATH.channel + '/mcsmambo.p',
                params: params,
                cache: false
            }).then(
                function success(response) {

                    var data = response;

                    defer.resolve(data);

                },
                function error(reason) {
                    defer.reject(reason);
                });

            return defer.promise;
        }

        return {
            save: save,
            retrieve: retrieve,
            getFullProduct: getFullProduct,
            getUser: getUser,
            getSalesAgent: getSalesAgent,
            getDataLayer: getDataLayer
        };
    }

})();