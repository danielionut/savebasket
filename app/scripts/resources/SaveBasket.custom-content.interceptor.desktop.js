/**
 * Intercepts the response from the textblock json and converts it to usable format
 */
(function(){
    'use strict';

    angular.module('SaveBasket.Resources')
        .factory('customContentHttpInterceptor', customContentHttpInterceptor)
        .config(['$httpProvider', function($httpProvider) {
            $httpProvider.interceptors.push(customContentHttpInterceptor);
        }]);

    function customContentHttpInterceptor() {

        var interceptor = {

            response: function(response) {
                if (response.config.url.indexOf('m5NextUrl=mdortext') !== -1) {
                    response.data = formatData(response.data);
                }
                return response;
            }

        };
        return interceptor;

        function formatData(data) {

            var obj = {};

            try {
                _.each(data.mdortext.tt_text, function(item) {
                    if (item.Text) {
                        obj[item.ID] = item.Text;
                    }

                });
            } catch(e) {

            }

            return obj;
        }

    }


})();
