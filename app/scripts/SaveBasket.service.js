(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name SaveBasket
     * @description Main factory for interacting with saveBasket features
     */
    angular
        .module('SaveBasket')
        .factory('SaveBasket', SaveBasketService);

    function SaveBasketService(sbOffers, $rootScope, SB_EVENT, sbUserLogin, $timeout, sbUser, sbSessionPersistence, SB_PERSISTENCE, SB_ENV, sbDataLayer) {

        var firstSave         = true,
            currentCommand    = '',
            saveAfterRetrieve = false;

        /**
         * Run this function to fake a login on CA
         */
        var fakeLogin = function () {
            console.log('Fake Login! Comment!');
            $timeout(function () {
                sbUser.set({
                    validation: {
                        extra: {
                            'customer.phone': '+31624534888',
                            'customer.code': '1.13435668',
                            'customer.email': 'san@san.com'
                        }
                    }
                });
            });

        };
        //fakeLogin();

        function initDesktop() {
            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.firstSave)) {
                firstSave = sbSessionPersistence.getItem(SB_PERSISTENCE.firstSave);
            }
        }

        if (SB_ENV.isDesktop) {
            initDesktop();
        }

        /**
         *
         */
        var saveOffer = function () {
            sbOffers.saveOffer();
        };

        /**
         * delete the offer passed
         */
        var deleteOffer = function (offer) {
            sbOffers.deleteOffer(offer);

            if (!sbOffers.offers.length) {
                $rootScope.$broadcast(SB_EVENT.modalClose);
            }
        };

        /**
         * Plots the offer passed into the function on the PDP
         * @param {object} offer - Object containing offer details.
         */
        var changeOffer = function (offer) {
            sbOffers.setSelectedOffer(offer);

            // don't close the modal on desktop... wait for the page to reload
            if (!SB_ENV.isDesktop) {
                $rootScope.$broadcast(SB_EVENT.modalClose);
            }
            sbOffers.navigateToOffer(offer);
        };
        var orderOffer  = function (offer) {
            sbOffers.setSelectedOffer(offer);

            // don't close the modal on desktop... wait for the page to reload
            if (!SB_ENV.isDesktop) {
                var listener = $rootScope.$on(SB_EVENT.closeModal, function () {
                    $rootScope.$broadcast(SB_EVENT.modalClose);
                    listener();
                });
            }
            sbOffers.orderOffer(offer);
        };

        /**
         * Retrieve offers for the current customer
         */
        var retrieveOffers = function () {
            sbOffers.retrieveOffers();
        };

        var retrieveAfterUserValidated = function () {
            // if we are redirected from portals login and need to save an offer, set saveAfterRetrieve to true.
            if (window.location.href.indexOf('savebasket=true') !== -1) {
                saveAfterRetrieve = true;
            }
            // after user validation retrieve the offers (if any)
            retrieveOffers();
        };

        /**
         * Event handlers
         */
        // @ToDo: Check the following listener. It seems to be redundant.
        // $rootScope.$on(SB_EVENT.saveOffer, function () {
        //     saveOffer();
        // });

        $rootScope.$on(SB_EVENT.currentCommand, function (event, command) {
            currentCommand = command;
        });

        $rootScope.$on(SB_EVENT.offersRetrieved, function () {
            if (sbOffers.offers.length) {
                firstSave = false;
            }
            if (saveAfterRetrieve) {

                $timeout(function () {
                    saveAfterRetrieve = false;
                    $rootScope.$broadcast(SB_EVENT.openBasket, 'save');
                }, 400);

            }
        });

        return {
            offerAvailable: sbOffers.offerAvailable,
            hasSelectedOffer: sbOffers.hasSelectedOffer,
            retrieveAfterUserValidated: retrieveAfterUserValidated,
            saveOffer: saveOffer,
            deleteOffer: deleteOffer,
            orderOffer: orderOffer,
            changeOffer: changeOffer,
            offerCount: function () {
                return sbOffers.getOfferCount();
            },
            login: sbUserLogin.login,
            maxOffers: sbOffers.getMaxOfferCount(),
            offers: sbOffers.offers,
            get firstSave() {
                return firstSave;
            },
            set firstSave(v) {
                firstSave = v;
                sbSessionPersistence.setItem(SB_PERSISTENCE.firstSave, firstSave);
            },
            currentCommand: currentCommand

        };
    }

})();
