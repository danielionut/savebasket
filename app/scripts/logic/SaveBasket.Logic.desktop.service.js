(function () {

    'use strict';

    angular.module('SaveBasket.Logic')
        .factory('sbLogic', SaveBasketLogicService);

    function SaveBasketLogicService(SB_ENV) {

        var showBasket = false;

        // on desktop the
        if (SB_ENV.isDesktop) {
            showBasket = true;
        }

        //$rootScope.$on('$viewContentLoaded', function(event) {
//
        //    try {
        //        showBasket = event.currentScope.$state.current.data.showSaveBasket;
        //    } catch(e) {
        //        /**
        //         * Should be false, but in order to always show the triggers!!
        //         * @type {boolean}
        //         */
        //        showBasket = false;
        //    }
//
        //    $rootScope.$broadcast(SB_EVENT.showBasket, showBasket);
        //});

        return {
            showBasket : showBasket
        };
    }

//
})();