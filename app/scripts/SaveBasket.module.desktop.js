(function () {
    'use strict';

    angular
        .module('SaveBasket', [

            'ngAnimate',
            'xml',
            'Kinetix.Widgets.CustomContent',

            'SaveBasket.Templates',

            'SaveBasket.User',
            'SaveBasket.Offers',
            'SaveBasket.Modal',
            'SaveBasket.Trigger',
            'SaveBasket.Constants',
            'SaveBasket.Logic',
            'SaveBasket.Resources',
            'SaveBasket.Session.Persistence',
            'SaveBasket.SalesChannel',
            'SaveBasket.DataLayer'



        ])
        .config(function(customContentProvider, SB_PATH) {
            customContentProvider.setOptions({
                endpointUrls: {
                    label: ['/', SB_PATH.channel,'/','mcsmambo.p?objecttype=rpc&m5NextUrl=mdortext&compatible=true'].join(''),
                    text: ['/', SB_PATH.channel,'/','mcsmambo.p?objecttype=rpc&m5NextUrl=mdortext&compatible=true'].join('')
                }
            });
        })
        .run(function($rootScope, sbUser, SaveBasket, SB_EVENT) {
            var userValidatedListener = $rootScope.$on(SB_EVENT.userValidated, function (event, user, doRetrieve) {
                doRetrieve = doRetrieve === undefined ? true : doRetrieve;
                if (doRetrieve) {
                    SaveBasket.retrieveAfterUserValidated();
                    userValidatedListener();
                }
            });
        });
})();