// Generated on 2015-03-24 using generator-angular 0.10.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Configurable paths for the application
    var appConfig = {
        app: require('./bower.json').appPath || 'app',
        dist: 'dist'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        yeoman: appConfig,

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            js: {
                files: ['<%= yeoman.app %>/scripts/**/*.js'],
                tasks: ['dev']
            },
            jsTest: {
                files: ['test/spec/{,*/}*.js'],
                tasks: ['newer:jshint:test', 'karma', 'dev']
            },
            gruntfile: {
                files: ['Gruntfile.js'],
                tasks: ['build']
            },
            sass: {
                files: ['<%= yeoman.app %>/**/*.scss'],
                tasks: ['dev']
            },
            html: {
                files: ['<%= yeoman.app %>/**/*.html'],
                tasks: ['dev']
            }
        },
        html2js: {
            mobile: {
                options: {
                    useStrict: true,
                    quoteChar: '\'',
                    htmlmin: {
                        collapseBooleanAttributes: true,
                        collapseWhitespace: true,
                        removeAttributeQuotes: false,
                        removeComments: true,
                        removeEmptyAttributes: true,
                        removeRedundantAttributes: false,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true
                    },
                    rename: function (moduleName) {
                        moduleName = moduleName.replace('../app/scripts/', '');
                        moduleName = moduleName.replace('/mobile', '');
                        return moduleName;
                    }
                },
                src: ['<%= yeoman.app %>/scripts/**/*.html', '!<%= yeoman.app %>/**/templates/desktop/*.*'],
                dest: '<%= yeoman.app %>/templates/templates.mobile.js',
                module: 'SaveBasket.Templates'
            },
            desktop: {
                options: {
                    useStrict: true,
                    quoteChar: '\'',
                    htmlmin: {
                        collapseBooleanAttributes: true,
                        collapseWhitespace: true,
                        removeAttributeQuotes: false,
                        removeComments: true,
                        removeEmptyAttributes: true,
                        removeRedundantAttributes: false,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true
                    },
                    rename: function (moduleName) {
                        moduleName = moduleName.replace('/desktop', '');
                        moduleName = moduleName.replace('../app/scripts/', '');
                        return moduleName;
                    }
                },
                src: ['<%= yeoman.app %>/scripts/**/*.html', '!<%= yeoman.app %>/**/templates/mobile/*.*'],
                dest: '<%= yeoman.app %>/templates/templates.desktop.js',
                module: 'SaveBasket.Templates'
            }
        },


        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: {
                src: [
                    'Gruntfile.js',
                    '<%= yeoman.app %>/scripts/**/*.js'
                ]
            }
        },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.dist %>',
                    src: '{,*/}*.css',
                    dest: '<%= yeoman.dist %>'
                }]
            }
        },

        // Performs rewrites based on filerev and the useminPrepare configuration
        usemin: {
            html: ['<%= yeoman.dist %>/{,*/}*.html'],
            css: ['<%= yeoman.dist %>/{,*/}*.css'],
            options: {
                assetsDirs: ['<%= yeoman.dist %>', '<%= yeoman.dist %>/images']
            }
        },

        uglify: {
            mobile: {
                files: {
                    '<%= yeoman.dist %>/save-basket-mobile.min.js': [
                        '<%= yeoman.dist %>/save-basket.mobile.js'
                    ]
                }
            },
            desktop: {
                files: {
                    '<%= yeoman.dist %>/save-basket.desktop.min.js': [
                        '<%= yeoman.dist %>/save-basket.desktop.js'
                    ]
                }
            },
            bower: {
                files: {
                    '<%= yeoman.dist %>/save-basket.vendor.min.js': [
                        '<%= yeoman.dist %>/save-basket.vendor.js'
                    ]
                }
            }
        },
        bower_concat: {
            all: {
                dest: '<%= yeoman.dist %>/save-basket.vendor.js',
                exclude: [
                    'jquery'
                ],
                dependencies: {
                    'angular-animate': 'angular',
                    'angular-bootstrap': ['bootstrap','angular'],
                    'angular-cookies': 'angular',
                    'angular-resource': 'angular',
                    'angular-xml': 'angular',
                    'bootstrap': 'angular',
                    'kinetix-angular-custom-content' : 'angular'

                }
            }
        },
        concat: {
            mobile: {
                src: [
                    '<%= yeoman.app %>/scripts/*.module.mobile.js',
                    '<%= yeoman.app %>/scripts/**/*.module.*',
                    '<%= yeoman.app %>/templates/*.mobile.*',
                    '<%= yeoman.app %>/scripts/**/*.js',
                    '!<%= yeoman.app %>/scripts/**/*.desktop.*'
                ],
                dest: '<%= yeoman.dist %>/save-basket.mobile.js'
            },
            desktop: {
                src: [
                    '<%= yeoman.app %>/scripts/*.module.desktop.js',
                    '<%= yeoman.app %>/scripts/**/*.module.*',
                    '<%= yeoman.app %>/templates/*.desktop.*',
                    '<%= yeoman.app %>/scripts/**/*.js',
                    '!<%= yeoman.app %>/scripts/**/*.mobile.*'
                ],
                dest: '<%= yeoman.dist %>/save-basket.desktop.js'
            }
        },
        ngAnnotate: {
            options: {
                // Task-specific options go here.
                singleQuotes: true
            },
            dist: {
                // Target-specific file lists and/or options go here.
                files: [
                    {
                        expand: true,
                        src: ['<%= yeoman.dist %>/save-basket.desktop.js', '<%= yeoman.dist %>/save-basket.desktop.js']
                    }
                ]
            }
        },
        clean: {
            dist: {
                files: [
                    {
                        dot: true,
                        src: [
                            '.tmp',
                            '<%= yeoman.dist %>/*.{css,js}',
                            '!<%= yeoman.dist %>/.git*'
                        ]
                    }
                ]
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    '<%= yeoman.dist %>/save-basket.mobile.css': '<%= yeoman.app %>/styles/save-basket.mobile.scss',
                    '<%= yeoman.dist %>/save-basket.desktop.css': '<%= yeoman.app %>/styles/save-basket.desktop.scss'
                }
            }

        },
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= yeoman.app %>/styles/assets',
                        dest: '<%= yeoman.dist %>/assets',
                        src: [
                            '*.png'
                        ]
                    }
                ]
            }
        },
        cssmin: {
            options: {
                expand: true,
                cwd: '<%= yeoman.dist %>'
            },
            target: {
                files: {
                    '<%= yeoman.dist %>/save-basket.desktop.min.css': ['<%= yeoman.dist %>/save-basket.desktop.css'],
                    '<%= yeoman.dist %>/save-basket.mobile.min.css': ['<%= yeoman.dist %>/save-basket.mobile.css']
                }
            }
        },
        compass: {
            dist: {
                options: {
                    config: 'config.rb'
                },
                files: {
                    '<%= yeoman.dist %>/save-basket.mobile.css': '<%= yeoman.app %>/styles/save-basket.mobile.scss',
                    '<%= yeoman.dist %>/save-basket.desktop.css': '<%= yeoman.app %>/styles/save-basket.desktop.scss'
                }
            }
        },
        ngdocs: {
            all: ['app/scripts/**/*.js']
        },
        jsdoc : {
            dist : {
                src: ['app/scripts/**/*.js'],
                options: {
                    destination: 'jsdoc'
                }
            }
        }
    });

    grunt.registerTask('dev', [
        'compass',
        'autoprefixer',
        'html2js',
        'newer:jshint:all',
        'concat',
        'copy'
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'dev',
        'cssmin',
        //'jsdoc',
        //'ngdocs',
        'ngAnnotate',
        'usemin',
        'bower_concat',
        'uglify'
    ]);

    grunt.registerTask('default', [
        'newer:jshint',
        'build'
    ]);
};
